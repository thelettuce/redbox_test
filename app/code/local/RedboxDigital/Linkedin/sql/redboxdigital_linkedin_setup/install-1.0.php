<?php
/* installer script - run when the module is installed */

$installer = $this;
$installer->startSetup();
$setup = Mage::getModel('customer/entity_setup', 'core_setup');

// add linkedin_profile customer field as an attribute
$setup->addAttribute('customer', 'linkedin_profile', array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'LinkedIn URL',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default' => '0',
    'visible_on_front' => 1,
        'source'=> 'profile/entity_linkedin_profile',
));

// add it to the customer attribute set
$customer = Mage::getModel('customer/customer');
$attrSetId = $customer->getResource()->getEntityType()->getDefaultAttributeSetId();
$setup->addAttributeToSet('customer', $attrSetId, 'General', 'linkedin_profile');

// add attribute to appropriate areas
Mage::getSingleton('eav/config')
->getAttribute('customer', 'linkedin_profile')
->setData('used_in_forms', array('adminhtml_customer','customer_account_create','customer_account_edit','checkout_register'))
->save();

$installer->endSetup();

?>