// Add custom validation to check for a string longer than 255 characters
// this was done specifically for the field linkedin url, which already uses the built-in validate-url check
//
// this checks for a string length of 255 characters
Validation.add('validate-linkedinurl','Linkedin URL must be a valid URL and not more than 255 characters long.',function(v) {
	
	var res=true;
	
	// check if string length is greater than 255
    if (v.length>255) { res=false; }

    return res;

});
