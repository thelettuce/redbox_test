# README #

This is my test answer for Redbox Digital Backend Developer Test

### What is this repository for? ###

* LinkedIn URL Field addition to customer registration form
* 1.0

### How do I get set up? ###

* Copy files to Magento base
* Login to Magento backend
* Go to System -> Configuration
* Under the ADVANCED tab click Advanced
* Make sure the RedboxDigital_Linkedin module is Enabled
* Log out and log back in again
* this should initiate the module
* to set whether the field is required or not - go to System -> Configuration -> ADVANCED tab -> System.
* that should be it!